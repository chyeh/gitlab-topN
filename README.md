# Run

```shell
$ go build
$ cp file.example.txt file.txt
$ ./gitlab-topN 3 # N=3
[9 8 8]
```


# Unit Test

```shell
$ go test
```


