package main

import (
	"container/heap"
	"math/big"
)

type PriorityQueue []*big.Int

func (pq PriorityQueue) Len() int {
	return len(pq)
}

func (pq PriorityQueue) Less(i, j int) bool {
	return pq[i].Cmp(pq[j]) == -1
}

func (pq PriorityQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
}

func (pq *PriorityQueue) Push(x interface{}) {
	item := x.(*big.Int)
	*pq = append(*pq, item)
}

func (pq *PriorityQueue) Pop() interface{} {
	old := *pq
	n := len(old)
	item := old[n-1]
	*pq = old[0 : n-1]
	return item
}

type BigIntQueue struct {
	pq PriorityQueue
}

func (biq *BigIntQueue) Push(x interface{}) {
	heap.Push(&biq.pq, x)
}

func (biq *BigIntQueue) Pop() interface{} {
	return heap.Pop(&biq.pq)
}

func (biq BigIntQueue) Len() int {
	return biq.pq.Len()
}

var BigInts = NewBigInts()

func NewBigInts() *BigIntQueue {
	var bigInts BigIntQueue
	heap.Init(&bigInts.pq)
	return &bigInts
}
