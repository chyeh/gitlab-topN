package main

import (
	"bufio"
	"fmt"
	"strings"
	"testing"
)

func TestTop(t *testing.T) {
	testcases := []*struct {
		input    string
		expected string
	}{
		{
			`2
4
5
3
1
6
7
2`, "[7 6 5 4 3]"},
		{`44
68
53
84
45
58
80
73`, "[84 80 73 68 58]"},
		{`9406356
9514178
5765624
3115958
2729424
4481252
9935142
8267369
6324038
102438`, "[9935142 9514178 9406356 8267369 6324038]"},
	}

	for _, c := range testcases {
		s := bufio.NewScanner(strings.NewReader(c.input))
		if fmt.Sprintf("%s", top(s, 5)) != c.expected {
			t.Error(c.input, c.expected)
		}
		t.Log(c.input, c.expected)
	}
}
