package main

import (
	"fmt"
	"math/big"
)

func strToBigInt(s string) (*big.Int, error) {
	i := new(big.Int)
	if _, b := i.SetString(s, 10); !b {
		return nil, fmt.Errorf("Convert string(\"%s\") to big integer error", s)
	}
	return i, nil
}

func reverseBigInts(q *BigIntQueue) []*big.Int {
	old := []*big.Int{}
	for BigInts.Len() != 0 {
		old = append(old, BigInts.Pop().(*big.Int))
	}

	updated := []*big.Int{}
	for i := len(old) - 1; i >= 0; i-- {
		updated = append(updated, old[i])
	}
	return updated
}
