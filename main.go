package main

import (
	"bufio"
	"fmt"
	"math/big"
	"os"
	"strconv"

	"github.com/sirupsen/logrus"
)

var logger = logrus.New()

func main() {
	if len(os.Args) != 2 {
		logger.Fatalln("Need a number N in the argument\n")
	}
	n, err := strconv.ParseInt(os.Args[1], 10, 32)
	if err != nil {
		logger.Fatalf("Cannot parse N from argument: [%v]\n", os.Args[1])
	}
	fp, err := os.Open("file.txt")
	if err != nil {
		logger.Fatalln(err)
	}
	defer fp.Close()

	s := bufio.NewScanner(fp)
	fmt.Println(top(s, int(n)))
}

func top(s *bufio.Scanner, n int) []*big.Int {
	for s.Scan() {
		bi, err := strToBigInt(s.Text())
		if err != nil {
			logger.Panic(err)
		}
		if BigInts.Len() < n {
			BigInts.Push(bi)
		} else {
			BigInts.Push(bi)
			BigInts.Pop()
		}
	}

	if err := s.Err(); err != nil {
		logger.Panic("Scanning error:", err)
	}

	return reverseBigInts(BigInts)
}
